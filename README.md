# Adaptec RAID template for Zabbix 5.0

Шаблон для Zabbix'а версии 5.0 и выше для опроса состояния RAID-контроллера Adaptec с помощью утилиты arcconf. Для обработки вывода команды arcconf используется встроенный в zabbix-server JavaScript-интерпретатор и зависимые элементы. Обновление значений всех элементов шаблона производится одним запросом к контроллеру.

###### Ограничения

Реализована работа только с одним контроллером на хосте. Ограничение может быть обойдено путем дублирования хоста и указания номера опрашиваемого контроллера в макросах.

Не опрашивает S.M.A.R.T.-атрибуты отдельных дисков

###### Содержимое проекта

- adaptec_raid_template.xml - шаблон для Zabbix'а

- userparameter_arcconf.conf - файл пользовательских параметров для агента.

###### Изображения

Latest data

![==](images/Latest_Data_sample.png "Latest Data sample")

Problems

![==](images/Problem_sample.png "Problems sample")

###### Среда тестирования

- ОС Debian 9.13

- CLI arcconf 2.05 (B22932)

- Zabbix Server v 5.0.2

- Zabbix Agent2 v 5.0.2

- RAID-controller Adaptec 6805

##### Установка и настройки на опрашиваемом хосте

###### arcconf

Утилиту командной строки arcconf можно найти на сайте Adaptec'а в разделе поддержки, либо скачать по этой ссылке http://download.adaptec.com/raid/storage_manager/arcconf_v2_05_22932.zip

Полученный архив необходимо разархивировать, скопировать файл linux_x64/static_arcconf/cmdline/arcconf в папку, из которой он будет исполняться, и предоставить ему права на исполнение. 

```shell
wget http://download.adaptec.com/raid/storage_manager/arcconf_v2_05_22932.zip
unzip arcconf_v2_05_22932.zip
cp -p linux_x64/static_arcconf/cmdline/arcconf /usr/sbin/
chmod +x /usr/sbin/arcconf
```

Выполнить проверку опроса RAID-контроллера

```shell
arcconf GETCONFIG 1
```

В ответ должна поступить почти полная информация о конфигурации и состоянии RAID'а:

```
Controllers found: 1
----------------------------------------------------------------------
Controller information
----------------------------------------------------------------------
   Controller Status                        : Optimal
   Channel description                      : SAS/SATA
   Controller Model                         : Adaptec 6805
   Controller Serial Number                 : 3B05130559B
   Controller World Wide Name               : 50000D11062F3D80
   Controller Alarm                         : Enabled
   Physical Slot                            : 3
   Temperature                              : 60 C/ 140 F (Normal)
   Installed memory                         : 512 MB
   Host bus type                            : PCIe
   Host bus speed                           : 5000 MHz
   Host bus link width                      : 8 bit(s)/link(s)
   Global task priority                     : High
   Performance Mode                         : Default/Dynamic
   PCI Device ID                            : 651
   Stayawake period                         : Disabled
   Spinup limit internal drives             : 0
   Spinup limit external drives             : 0
   Defunct disk drive count                 : 0
   NCQ status                               : Enabled
   Statistics data collection mode          : Enabled
.
.
.

Command completed successfully.
```

###### userparameter

Для вызова утилиты командной строки агентом, необходимо добавить соответствующий пользовательский параметр. Для этого необходимо поместить файл userparameter_arcconf.conf в папку пользовательских параметров агента. Обычно это /etc/zabbix/zabbix_agent.d/ (или /etc/zabbix/zabbix_agent2.d/ для агента второго поколения). Псоле чего надо перезапустить zabbix-агент.

###### sudo

Опрос состояние RAID-контроллера производится от суперпользователя. Чтобы пользователь zabbix так же имел право на выполнение опроса, его необходимо добавить в группу суперпользователей (wheel или sudo) или через visudo добавить следующую строку

```
zabbix ALL=NOPASSWD:/usr/sbin/arcconf
```

##### Настройки сервера

###### Шаблон

Для добавления шаблона на сервер необходимо перейти Configuration -> Templates, нажать кнопку Import, выбрать файл шаблона adaptec_raid_template.xml и загрузить его на сервер, нажав Import в нижней части формы. 

После этого будет возможно присоединять шаблон с именем Template Adaptec RAID к хостам для опроса имеющихся на них RAID-контроллеров Adaptec.

###### Настрока макросов

В шаблоне объявлен только один макрос {$CONTROLLER_NUMBER} со значением по умолчанию "1". Значение этого макроса передается на опрашиваемый хост команде arcconf  в качестве номера опрашиваемого контроллера.

---

Спасибо [Yuriy Smetana](https://github.com/YSmetana) за идею шаблона